# Learning by doing - [deusops](https://deusops.com/classbook)

**Личные заметки в целях обучения,а не призыв к действию:**

<details>
  <summary>Light</summary>

## Вариант реализации:

# dev01:

- на базе виртуальной машины в Яндекс Облаке, используя следующий образ:

![img](img/1.png)

- Обновляем списки пакетов и устанавливаем пакеты необходимые для работы:

```bash
apt-get update && apt-get install -y git vim-console docker-{ce,compose}
```

- Запускаем и добавляем в автозагрузку **docker**:

```bash
systemctl enable --now docker.service
```

- Переходим в директорию **/opt** и выполняем клонирование репозитория c проектом:

```bash
cd /opt
```

```bash
git clone https://gitlab.deusops.com/deuslearn/classbook/todo-list-app.git
```

- Переходим в директоию с проектом и описываем **Dockerfile**:

```bash
cd todo-list-app/
```

```bash
vim Dockerfile
```

Содержимое:

![img](img/2.png)

### *Почему две инcтрукции **COPY** - а не одна **COPY ./ ./**?*

- потому что в случае изменения кодовой базы, хотелось бы взять из **CACHED** слои в котором уже выполнены установки необходимых зависимостей через **yarn install**, а не каждый раз, когда произойдёт изменение кодовой базы - ожидать пока установится всё через **yarn**

## Проведём тестирования скорости сборки:

- Выполним сборку Docker-образа из текущего **Dockerfile**:

```bash
docker build -t test_speed:v0.1 .
```

- Приведём **Dockerfile** к следующему виду:
    - считаем что закомментированных строк просто нет, и на этапе **COPY** копируем сразу всё в образ:

![img](img/3.png)

- Выполним сборку Docker-образа:

```bash
docker build -t test_speed:v0.2 .
```

Результат:

![img](img/4.png)

- Вносим несущественное изменение в кодовую базу, например в файл **src/index.js**:

![img](img/5.png)

- Выполним сборку Docker-образа:

```bash
docker build -t test_speed:v0.2 .
```

Результат:
- таким образом, поправив всеголишь кодовую базу - мы должны ждать установку зависимостей, которые никак не поменялись, и так будет каждый раз

![img](img/6.png)

- Возвращаем **Dockerfile** к прежнему виду руководствуясь - **best practices**:

![img](img/7.png)

- Выполним сборку Docker-образа:

```bash
docker build -t todo-list-app:0.0.1 .
```

Результат:
- разница в скорости сборки заметна в разы - засчёт кеширования слоя в котором уже установленны всё что необходимо через **yarn**

![img](img/8.png)

- Выполняем запуск приложения и проверяем работоспособность базовую:

```bash
docker run --rm -it todo-list-app:0.0.1
```

Результат:

![img](img/9.png)

- Напишем **docker-compose.yml** для запуска приложения с учетом сервисов, необходимых для запуска и сохранения данных:

```bash
vim docker-compose.yml
```

Содержимое:

![img](img/10.png)

- Создадим директорию **www-data**:

```bash
mkdir www-data
```

- Напишем конфигурационный файл для веб-сервера **nginx**, который будет выступать в роли **reverse-proxy**:

```bash
vim www-data/nginx.conf
```

Содержимое:

![img](img/11.png)

- Также необходимо сгенирировать или положить в **www-data** сертификаты:

![img](img/12.png)

- Опишем файл **.dockerignore** - чтобы не тянуть лишнего в образ с приложением:

```bash
vim .dockerignore
```

Содержимое:

![img](img/13.png)

- Выполняем сборку образа приложения и запуск приложения с учетом сервисов, необходимых для запуска и сохранения данных:

```bash
docker-compose up -d
```

Результат:

![img](img/14.png)

- Проверяем связность с базой данных **mysql**:

```bash
docker logs app
```

Результат:

![img](img/15.png)

- Проверяем доступность приложения по **https://dev.example.com**:

![img](img/16.png)

- Проверяем сохранность данных:
    - добавляем пару задач

![img](img/17.png)

- Останавливаем контейнеры и удаляем всё:

```bash
docker rm -f $(docker ps -qa)
```

Результат:

![img](img/18.png)

![img](img/19.png)

- Запускаем приложения через docker-compose ещё раз:

```bash
docker-compose up -d
```

Результат:

![img](img/20.png)

![img](img/21.png)

- В **https://hub.docker.com** - создадим репозиторий для публикации образа с приложением:

![img](img/22.png)

- Также создаём **access token** - для последующего логина:

![img](img/23.png)

- Назначим соответствующий **тег** для образа с приложением:

```bash
docker tag todo-list-app_app:latest newerr0r/todo-list-app:latest
```

- Выполняем логин используя ранее созданный **access token**:

```bash
docker login  -u newerr0r
```

Результат:

![img](img/24.png)

- Загружаем образ с приложением в репозиторий:

```bash
docker push newerr0r/todo-list-app:latest
```

![img](img/25.png)

![img](img/26.png)

# prod01:

- на базе виртуальной машины в Яндекс Облаке, используя следующий образ:

![img](img/1.png)

- Обновляем списки пакетов и устанавливаем пакеты необходимые для работы:

```bash
apt-get update && apt-get install -y vim-console docker-ce nginx MySQL-server
```

- Запускаем и добавляем в автозагрузку **docker**:

```bash
systemctl enable --now docker.service
```

- Запускаем и добавляем в автозагрузку **mysqld**:

```bash
systemctl enable --now mysqld
```

- Задаём пароль для пользователя "**root**" в MySQL:

```bash
mysql -u root
```

```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'secret';
```

где:

**secret** - пароль для входа в MySQL под пользователем "**root**"

- таким образом, доступ к MySQL без ввода пароля больше не пройдёт:

![img](img/27.png)

- Создаём базу данных для приложения с именём **todos**:

```
CREATE DATABASE todos;
```

- Создаём пользователя для приложения с именем **todo-user** с паролем **todo-pass**:
    - *знак **процента** означает, что пользователь может подключаться к серверу с любого хоста*

```
CREATE USER 'todo-user'@'%' IDENTIFIED BY 'todo-pass';
```

- Выдадим созданному пользователю (**todo-user**) полные права на созданную базу данных (**todos**):

```
GRANT ALL PRIVILEGES ON todos.* TO 'todo-user'@'%' WITH GRANT OPTION;
```

Проверяем:

![img](img/28.png)

- Разрешаем доступ к MySQL из сети:

```bash
sed -i "s/skip-networking/#skip-networking/g" /etc/my.cnf.d/server.cnf
```

```bash
systemctl restart mysqld
```

Проверить:

![img](img/29.png)

- Выполняем запуск приложения из образа ранее закруженного в **https://hub.docker.com** для проверки работоспособности и связи с локальной базой данных:
    - указываем необходимые переменные для подключения к БД:


```bash
docker run --rm -it -e MYSQL_HOST=10.131.0.22 -e MYSQL_USER=todo-user -e MYSQL_PASSWORD=todo-pass -e MYSQL_DB=todos newerr0r/todo-list-app:latest
```

Результат:

![img](img/30.png)

- Опишем конфигурационный файл для **reverse-proxy**:

```bash
vim /etc/nginx/sites-available.d/todo-proxy.conf
```

Содержание:

![img](img/31.png)

- Создаём директорию для сертификатов и размещаем их в ней:
    - *генерация сертификатов не рассматривается*

```bash
mkdir /etc/nginx/ssl
```

Результат:

![img](img/32.png)

- Добавляем символьную ссылку на конфигурационный файл для **nginx**:

```bash
ln -s /etc/nginx/sites-available.d/todo-proxy.conf /etc/nginx/sites-enabled.d/
```

- Проверяем корректность конфигурационного файла для **nginx**:

![img](img/33.png)

- Перезапускаем службу **nginx**:

```bash
systemctl restart nginx
```

- Создадим файл, в котором опишим переменные которые необходимо передавать контейнеру при запуске для подключения к базе данных:

```bash
vim db_connection.env
```

Содержимое:

![img](img/34.png)

- Запускаем контейнер с приложением:

```bash
docker run -d --restart always --name todo-list-app -p 127.0.0.1:3000:3000 --env-file ./db_connection.env newerr0r/todo-list-app:latest
```

Результат:

![img](img/35.png)

- Проверяем доступность приложения по **https://example.com**:

![img](img/36.png)

</details>
