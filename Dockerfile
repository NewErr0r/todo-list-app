FROM node:22-alpine
WORKDIR /todo-list-app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
EXPOSE 3000
ENTRYPOINT ["node"]
CMD ["src/index.js"]
